/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../NSWPRDValAlg.h"
#include "../MuonTPMetaDataAlg.h"
DECLARE_COMPONENT( NSWPRDValAlg )
DECLARE_COMPONENT( MuonVal::MuonTPMetaDataAlg )


